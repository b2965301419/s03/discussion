# Lists, Dictionaries, Functions, Classes

# Python has several structures to store collections on multiple items inside a single variable
# Lists, Dictionaries, Tuples, Sets

# [Section] Lists
# Lists are similar to arrays in JS
names = ["John", "Paul", "George", "Ringo"] # string list
programs = ['developer career', 'pi-shape', 'short courses'] # string list
durations = [260, 180, 20] # number list
truth_values = [True, False, True, True, False] # boolean list

# Having multiple data types inside a list is not recommended because it could lead to confusion as to what is the relationship of the elements
sample_list = ["Apple", 3, False, 'Potato', 4, True]
print(names)
print(sample_list) 

# getting the size of the list
# len()
print(len(programs))

# Accessing of values
# Lists can be accessed by providing the index number of the element
# Accessing the first element
print(programs[0])
# Acccessing the second element
print(durations[1])
# Accessing the last item using negative index
print(programs[-1])
# Using negative index
print(names[-2])

# Printing the whole list
print(programs)

# Accessing a range of index values
# list_name[ start_index : end_index ]
print(programs[0:2]) # index 0 - index 1

# Mini-activity
students = ["naruto", "sasuke", "sakura", "sai", "kakashi"]
grades = ['A', 'B', 'C', 'D', 'E']

count = 0
while count < len(students):
	print(f"The grade of {students[count]} is {grades[count]}")
	count += 1

# [Section] List Manuplation
# Updating the lists
print(f"Current value: {programs[2]}")
programs[2] = 'Short Courses'
print(f"Current value: {programs[2]}")

# Methods - Can be used to manipluate the elements within 
# Adding of elements - append()
programs.append('global')
print(programs)

# Deleting of items
# Add items to be deleted
durations.append(360)
print(durations)
# Deleting the last item in the list
del durations[-1]
print(durations)

# Membership checks
# "in" keyword checks if the element is in the specified list
# returns a boolean value
print(20 in durations) # True
print(500 in durations) # False

# Sorting
# sort() - sorts the list alphanumerically, in an ascending order, by default
print(names)
names.sort()
print(names)

# Emptying the list
# clear() - Empties the contents of the list
test_list = [1, 2, 3, 4, 5]
print(test_list)
test_list.clear()
print(test_list)

# del test_list[1]
# print(test_list)
# del test_list
# print(test_list)

# [Section] Dictionaries
# Are used to store data in key: value pairs. This is similar to objects in JS
# They are ordered, changeable, and does not allow duplicates
# Ordered - Have defined order that cannot be changed
# To create a dictionary, the {} is used and key-value pairs are denoted with key:value

person1 = {
	"name" : "Brandon",
	"age" : 28,
	"occupation" : "student",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "Graph"]
}
print(person1)

print(person1["name"])
print(person1["age"])
# print(person1.name) - WILL NOT WORK

# Size of the dictionary - Denoted by the number of key-value pairs in the dictionary
print(len(person1))
# del person1["age"]
# print(person1)
# print(len(person1))

# In this part, to get keys or values or items, we should use dot(.) notation
# Get all the keys of a dictionary
print(person1.keys()) 
print("--------------------------")

# Get all the values of a dictionary
print(person1.values())
print("--------------------------")

# Get all the items (key-value pairs) of a dictionary
print(person1.items())
print("--------------------------")

# Adding key-value pair
person1["nationality"] = "Indian"
person1.update({"fave_food" : "Dosa"})
print(person1)
print(person1.items())
print("-----------------------")

# Deleting key-value pair
person1.pop("fave_food")
del person1["nationality"]
print(person1)
print(person1.items())
print("-----------------------")

Car = {
	"brand" : "Volkswagen",
	"model" : "Bugatti Veyron EB 16.4",
	"year_of_making" : 2015,
	"color" : "Blue"
}

print(f"I own a {Car['brand']} {Car['model']} and it was made in {Car['year_of_making']}")

# clear() empties the dictionary
# Adding a dictionary to be cleared
person2 = {
	"name" : "John",
	"age" : 18
}
print(person2)
person2.clear()
print(person2)

# Nested dictionaries
classroom = {
	"student1" : person1,
	"student2" : person2
}
print(classroom)
print("---------------------------------")

# [Section] Functions
# functions are blocks of code that runs when called 
# can be used to get inputs, process them, and return outputs
# SYNTAX
# def <function_name>():
#			codes_to_be_executed
def my_greeting():
	print('Hello!')
my_greeting()

# Parameters in a function
def greet_user(user):
	print(f"Hello, {user}!")
greet_user("Brother")
greet_user("Bitch")

# "return" keyword allows the function to return values
def addition(num1, num2):
	return num1 + num2
total = addition(5, 10)
print(total)

# [Section] Lamda function
# A small, anonymous function that can be used for callbacks
# It is just like any normal Python function, except that it has not name when defined and it contains in one line of code
# A lambda function can take any number of arguments, but can only have one expression
# variable_ name = lambda <parameters> : expression
greeting = lambda person : f'Hello {person}'
print(greeting("Sergeant"))
print(greeting("Useless"))

mult = lambda a, b : a*b
print(mult(2,3))
print(mult(69,96))
# product = mult(69,96)
# print(product)
print("------------------------------")

# [Section] Classes
# Blueprint to describe the concept of objects
# Each object has characteristics (properties) and behaviours (methods)
class Car():
	# def __init__() - Used to create properties in a class
	# Whenever we define properties and methods in python classes, we always have to supply "self" as parameter
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self. year_of_make = year_of_make

		# other properties
		self.fuel = "Gasoline"
		self.fuel_level = 0
		self.repairing_cost = 10000


	# methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print('filling up the fuel tank')
		self.fuel_level = 100
		print(f'New fuel level : {self.fuel_level}')

	def repair_car(self):
		print("Repairs needed!")
		print(f'recieved {self.repairing_cost} for repairing')
		print("Repairing done!")

	def drive(self, distance):
		print(f'The car has driven {distance} kilometers')
		print(f'The fuel level is now {self.fuel_level - (distance/8)}')

new_car = Car("Nissan", "GT-R", 2019)
print(f"My new car is {new_car.brand} {new_car.brand} {new_car.year_of_make}")
new_car.fill_fuel()
new_car.repair_car()
print("Car is fresh as new")
new_car.drive(50)